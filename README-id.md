# Pegiatan
[![Status penerjemahan](https://translate.codeberg.org/widgets/pegiatan/-/pegiatan/svg-badge.svg)](https://translate.codeberg.org/engage/pegiatan/)

## Apa itu Pegiatan?
Pegiatan adalah sebuah aplikasi web sederhana untuk membuat tugas-tugas yang harus dilakukan semakin seru. Seperti sistem *quest* atau misi dalam berbagai permainan, kamu bisa membuat tugasmu sebagai misi yang harus kamu selesaikan untuk mendapatkan hadiah!

## Terjemahan
Bantu menerjemahkan aplikasi ini di Codeberg Translate!

[![Status penerjemahan](https://translate.codeberg.org/widgets/pegiatan/-/pegiatan/multi-auto.svg)](https://translate.codeberg.org/engage/pegiatan/)